FROM jupyter/minimal-notebook:1386e2046833
USER root
RUN rm /bin/sh && ln -s /bin/bash /bin/sh
RUN apt-get update \
    && apt-get install -yq --no-install-recommends nodejs npm \
    && npm install -g --silent --unsafe-perm ijavascript \
    && ijsinstall --install=global \
    && chmod -R 777 .local \
    && conda create -n xeus python=3.6 jupyterlab -c conda-forge \
    && source activate xeus \
    && conda install xeus-cling -c QuantStack -c conda-forge
USER $NB_UID
CMD source activate xeus && jupyter-notebook
